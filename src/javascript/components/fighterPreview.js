import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const { name, health, defense, attack } = fighter;
  const imgElement = createFighterImage(fighter);
  const fighterNameInfoElement = createElement({
    tagName: 'span',
    className: 'fighter-preview___info',
  });
  fighterNameInfoElement.innerHTML = `Name: ${name}`;
  const fighterHealthInfoElement = createElement({
    tagName: 'span',
    className: 'fighter-preview___info',
  });
  fighterHealthInfoElement.innerHTML = `Health: ${health}`;
  const fighterDefenseInfoElement = createElement({
    tagName: 'span',
    className: 'fighter-preview___info',
  });
  fighterDefenseInfoElement.innerHTML = `Defense: ${defense}`;
  const fighterAttackInfoElement = createElement({
    tagName: 'span',
    className: 'fighter-preview___info',
  });
  fighterAttackInfoElement.innerHTML = `Attack: ${attack}`;
  fighterElement.append(imgElement,
    fighterNameInfoElement,
    fighterHealthInfoElement,
    fighterDefenseInfoElement,
    fighterAttackInfoElement);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
