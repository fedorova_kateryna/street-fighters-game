import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function
  const modalInfo = {
    title: `The winner is ${fighter.name}`,
    bodyElement: createElement({ tagName: 'div', className: 'modal-body' }),
    onClose: () => {}
  };
  showModal(modalInfo);
}
