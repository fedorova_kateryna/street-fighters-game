import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;
  const firstFighterHealthBar = document.querySelector('.arena___health-bar');
  const secondFighterHealthBar = document.querySelectorAll('.arena___health-bar')[1];
  let winner, timePlayerOneCriticalHit, timePlayerTwoCriticalHit;
  let pressedPlayerOneCriticalHit = new Set();
  let pressedPlayerTwoCriticalHit = new Set();
  document.addEventListener('keyup', function(event) {
    if (event.code === controls.PlayerOneAttack) {
      const hitDamage = getDamage(firstFighter, secondFighter);
      if (hitDamage > 0) {
        secondFighterHealth -= hitDamage;
        secondFighterHealthBar.style.width = `${secondFighterHealth / (secondFighter.health) * 100}%`;
      }
    } else if (event.code === controls.PlayerTwoAttack) {
      const hitDamage = getDamage(firstFighter, secondFighter);
      if (hitDamage > 0) {
        firstFighterHealth -= hitDamage;
        firstFighterHealthBar.style.width = `${firstFighterHealth / (firstFighter.health) * 100}%`;
      }
    }
  });
  document.addEventListener('keydown', function(event) {
    pressedPlayerOneCriticalHit.add(event.code);
    for (let code of controls.PlayerOneCriticalHitCombination) {
      if (!pressedPlayerOneCriticalHit.has(code)) {
        return;
      }
    }
    pressedPlayerOneCriticalHit.clear();
    if (timePlayerOneCriticalHit === undefined) {
      timePlayerOneCriticalHit = +(new Date());
      secondFighterHealth -= firstFighter.attack * 2;
      secondFighterHealthBar.style.width = `${secondFighterHealth / (secondFighter.health) * 100}%`;
    } else {
      let time = +(new Date()) - timePlayerOneCriticalHit;
      if (time >= 10000) {
        timePlayerOneCriticalHit = +(new Date());
        secondFighterHealth -= firstFighter.attack * 2;
        secondFighterHealthBar.style.width = `${secondFighterHealth / (secondFighter.health) * 100}%`;
      }
    }
  });
  document.addEventListener('keyup', function(event) {
    pressedPlayerOneCriticalHit.delete(event.code);
  });
  document.addEventListener('keydown', function(event) {
    pressedPlayerTwoCriticalHit.add(event.code);
    for (let code of controls.PlayerTwoCriticalHitCombination) {
      if (!pressedPlayerTwoCriticalHit.has(code)) {
        return;
      }
    }
    pressedPlayerTwoCriticalHit.clear();
    if (timePlayerTwoCriticalHit === undefined) {//
      timePlayerTwoCriticalHit = +(new Date());//
      firstFighterHealth -= secondFighter.attack * 2;
      firstFighterHealthBar.style.width = `${firstFighterHealth / (firstFighter.health) * 100}%`;
    } else {
      let time = +(new Date()) - timePlayerTwoCriticalHit;//
      if (time >= 10000) {
        timePlayerTwoCriticalHit = +(new Date());//
        firstFighterHealth -= secondFighter.attack * 2;
        firstFighterHealthBar.style.width = `${firstFighterHealth / (firstFighter.health) * 100}%`;
      }
    }
  });
  document.addEventListener('keyup', function(event) {
    pressedPlayerTwoCriticalHit.delete(event.code);
  });
  //еще бойцы могут блокировать удары с помощью клавиш D и L соответственно, в таком случае боец ​​уклоняется от удара.
  if (secondFighterHealth <= 0) {
    secondFighterHealthBar.style.width = '0';
    winner = firstFighter;
  }
  if (firstFighterHealth <= 0) {
    firstFighterHealth.style.width = '0';
    winner = secondFighter;
  }
  if (winner) {
    return await new Promise((resolve) => { //await
      // resolve the promise with the winner when fight is over
      resolve(winner);
    });
  }
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage > 0) {
    return damage;
  } else {
    return 0;
  }
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
